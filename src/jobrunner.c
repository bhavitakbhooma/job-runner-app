#include <ctype.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/time.h>
#include <fcntl.h>
#include <signal.h>
#include <csse2310a3.h>
#include "pipeLibrary.h"
#include "errorHandling.h"

#define EXEC_FAIL 255
#define ASCII_TAB 9
#define ASCII_SPACE 32

// Structure to store fields of the jobfile.
typedef struct JobFileFields {
    char* name;
    char* stdin;
    char* stdout;
    int timeout;
    char** args;
    int buf;
    int jobNumber;
} JobFileFields;

// Structure to store Running Job details.
typedef struct {
    JobFileFields* job;
    pid_t pid;
    bool completed;
    int exitStatus;
} RunningJob;

//Following global variables are used to handle signal.
// Keeps track of waiting jobs.
static RunningJob* jobsWaiting = NULL;
// Keeps a count of job.
static int jobCount = -1;
// String array used in signal handling to store exit messages.
static char exitMessages[1000][100];

// Accepts job as @param.
// Displays job according to verbose mode.
void verbose_mode_display(JobFileFields* job) {
    fprintf(stderr, "%d:%s:%s:%s:%d", job->jobNumber, job->name,
            job->stdin, job->stdout, job->timeout);
    for (int i = 0; i < job->buf; i++) {
        fprintf(stderr, ":%s", job->args[i]);
    }
    fputc('\n', stderr);
}

// Checks if at least 2 arguments provided
// Checks if -v is present in the first place
bool check_args(int argc, char** argv) {
    if (argc < 2) {
        usg_err();
    }
    bool vFlag = false;
    for (int i = 1; i < argc; i++) {
        if (!strcmp(argv[i], "-v")) {
            vFlag = true;
            if (i != 1) {
                usg_err();
            }
        } 
    } 
    return vFlag;
}

// Takes jobfile fieldName as a @param.
// Checks if '@' is present before any pipe name in the jobfile.
bool is_at_char(char* fieldName) {
    return strlen(fieldName) && fieldName[0] == '@';
}

// Takes jobfile line as a @param.
// Checks if the line is a comment which starts with '#'
bool is_comment(char* line) {
    return strlen(line) && line[0] == '#';
}

// Takes jobfile line as a @param.
// Checks if the line is empty lines, 
// including lines with only space or tab characters
bool is_empty(char* line) {
    return (!strcmp(line, "") || 
            (int) line[0] == ASCII_SPACE || (int) line[0] == ASCII_TAB);  
}

// Takes a string as a @param.
// Calculates the size of a string and returns it.
int strings_size(char** str) {
    int size;
    for (size = 0; str[size]; size++) {
        // calculates size
    } 
    return size;
}

// Takes a string as a @param.
// Converts a string to integer. 
int from_string_int(char* str) {
    if (!str || !strlen(str)) {
        return 0;
    }
    for (int i = 0; i < strlen(str); i++) {
        if (!isdigit(str[i])) {
            return -127;
        }
    }
    return atoi(str);
}

// Takes jobfile line and the fields as @param.
// Splits line in the jobfile in different fields. 
// If the line has <3 fields returns false else splits the line by commas.
// Also checks if value of timeout is <0, if yes then returns false.
// Assigns respective fields to program name, stdin, stdout, timeout and args.
// Returns true
bool check_jobfile(char* line, JobFileFields* fieldName) {
    char** splitLine = split_by_commas(line);
    int size = strings_size(splitLine);
    if (size < 3) {
        return false;
    }
    for (int i = 0; i < 3; i++) {
        char* str = splitLine[i];
        if (!strlen(str)) {
            return false;
        }
    }
    int timeout = from_string_int(splitLine[3]);
    if (timeout < 0) {
        return false;
    }
    char** args = NULL;
    int buf = 0;
    if (size >= 5) {
        buf = size - 4;
        args = malloc(sizeof(char *) * buf);
        for (int i = 0; i < buf; i++) {
            args[i] = strdup(splitLine[i + 4]);
        }
    }
    static int jobNumbers = 1;
    fieldName->jobNumber = jobNumbers;
    jobNumbers++;
    fieldName->name = strdup(splitLine[0]);
    fieldName->stdin = strdup(splitLine[1]);
    fieldName->stdout = strdup(splitLine[2]);
    fieldName->timeout = timeout;
    fieldName->buf = buf;
    fieldName->args = args;
    return true;
}

// Takes jobfile and count as @param.
// Opens the file to read each line.
// Checks if the line is empty or is a comment and ignores it.
// Iterates linenum count and returns the job.
JobFileFields* process_files(char** files, int* count) {
    int capacity = 10;
    int c = 0;
    JobFileFields* jobs = malloc(sizeof(JobFileFields) * capacity);
    for (int i = 0; files[i]; i++) {
        FILE* file = fopen(files[i], "r");
        if (!file) {
            invalid_file(files[i]);
        }
        int lineNum = 1;
        char* line;
        while ((line = read_line(file))) {
            if (is_empty(line) || is_comment(line)) {
                free(line);
                lineNum++;
                continue;
            }
            if (!check_jobfile(line, &jobs[c])) {
                invalid_line(lineNum, files[i]);
            }
            c++;
            lineNum++;
            free(line);
            if (c == capacity) {
                capacity += 10;
                jobs = realloc(jobs, sizeof(JobFileFields) * capacity);
            }
        }
        fclose(file);
    }
    *count = c;
    return jobs;
}

// Accepts a string @param and returns true if string doesnt contain '-'.
bool io_jobrunner(char* inout) {
    return !strcmp("-", inout);
}

// Accepts a string @param and returns true if string doesnt start with '@'.
bool io_pipe(char* inout) {
    return strlen(inout) && inout[0] == '@';
}

// Accepts a string @param and returns true if string doesnt contain 
// '-' or doesnt start with '@'.
bool io_file(char* inout) {
    return strlen(inout) && !io_jobrunner(inout) && !io_pipe(inout);
}

// Accepts a job as @param.
// Checks if stdin field can be opened to read. 
// If not prints unable to read file error and returns false.
bool valid_file_in(JobFileFields* job) {
    if (io_file(job->stdin)) {
        FILE* checkFile = fopen(job->stdin, "r");
        if (!checkFile) {
            error_stdin(job->stdin);
            return false;
        }
        fclose(checkFile);
    }
    return true;
}

// Accepts a job as @param.
// Checks if stdin field can be opened to write. 
// If not prints unable to write file error and returns false.
bool valid_file_out(JobFileFields* job) {
    if (io_file(job->stdout)) {
        FILE* checkFile = fopen(job->stdout, "w");
        if (!checkFile) {
            error_stdout(job->stdout);
            return false;
        }
        fclose(checkFile);
    }
    return true;
}

// Accepts jobs and count as @param.
// Validates pipe inputs and outputs.
// Checks if stdin and stdout is a pipe and an invalid.
JobFileFields** validate_pipe_io(JobFileFields** jobs, int c, int* count) {
    int n = 0;
    JobFileFields** result = malloc(sizeof(JobFileFields *) * c);
    for (int i = 0; i < c; i++) {
        if ((io_pipe(jobs[i]->stdin) && check_invalid_pipe(jobs[i]->stdin)) ||
                (io_pipe(jobs[i]->stdout) 
                && check_invalid_pipe(jobs[i]->stdout))) {
            continue;
        }
        result[n++] = jobs[i];
    }
    *count = n;
    return result;
}

// Checks if the stdout is a what we are looking for. 
bool search_specific_stdout(char* stdOut, JobFileFields** fieldName, int n) {
    for (int i = 0; i < n; i++) {
        if (io_pipe(fieldName[i]->stdout) && 
                !strcmp(stdOut, fieldName[i]->stdout)) {
            return true;
        }
    }
    return false;
}

// Takes validated jobs as @param.
// Creates a queue for validated jobs.
JobFileFields** create_queue(JobFileFields** validated, int c) {
    JobFileFields** result = malloc(sizeof(JobFileFields *) * c);
    memcpy(result, validated, sizeof(JobFileFields*) * c);
    return result;
}

// Takes queue as @param.
// Storing the jobs in a queue. 
JobFileFields* first_in_queue(JobFileFields** queue, int* c) {
    if (!*c) {
        return NULL;
    }
    JobFileFields* result = queue[0];
    for (int i = 1; i < *c; i++) {
        queue[i - 1] = queue[i];
    }
    int newC = (*c) - 1;
    *c = newC;
    return result;
}

// Takes queue and job as @param.
// Adding the job back to the queue.
void add_back_to_queue(JobFileFields** queue, int* c, JobFileFields* job) {
    queue[*c] = job;
    int newC = (*c) + 1;
    *c = newC;
}

// Takes validated job as @param.
// Sorts the jobs in order and according to their place in queue.
// Runs first_in_queue() and add_back_to_queue().
// Returns the job. 
JobFileFields** sort_jobs(JobFileFields** validated, int c) {
    int n = 0;
    JobFileFields** result = malloc(sizeof(JobFileFields *) * c);
    JobFileFields** queue = create_queue(validated, c);
    int queueSize = c;
    while (queueSize > 0) {
        JobFileFields* nextInQueue = first_in_queue(queue, &queueSize);
        if (io_pipe(nextInQueue->stdin) && 
                !search_specific_stdout(nextInQueue->stdin, result, n)) {
            add_back_to_queue(queue, &queueSize, nextInQueue);
        } else {
            add_back_to_queue(result, &n, nextInQueue);
        }
    }
    free(queue);
    return result;
}

// Takes the running jobs and count as @param.
// Validating file inputs and outputs.
// Checks if stdin and stdout is a file.
// Or else checks if the job is using pipes.
JobFileFields** validate_file_io(JobFileFields* jobs, int c, int* count) {
    int n = 0;
    JobFileFields** result = malloc(sizeof(JobFileFields *) * c);
    bool validFileIn, validFileOut;
    for (int i = 0; i < c; i++) {
        validFileIn = valid_file_in(&jobs[i]);
        validFileOut = valid_file_out(&jobs[i]);
        if (validFileIn && validFileOut) {
            result[n++] = &jobs[i];
        } else {
            // maybe this job using pipe
            if (io_pipe(jobs[i].stdin) && !check_invalid_pipe(jobs[i].stdin)) {
                add_invalid_pipe(jobs[i].stdin);
            }
            if (io_pipe(jobs[i].stdout) && 
                    !check_invalid_pipe(jobs[i].stdout)) {
                add_invalid_pipe(jobs[i].stdout);
            }
        }
    }
    *count = n;
    return result;
}

// Takes the running jobs as @param.
// Starts the created pipe to execute input output process.
void start_pipes(JobFileFields** jobs, int c) {
    for (int i = 0; i < c; i++) {
        if (io_pipe(jobs[i]->stdin)) {
            process_pipe(jobs[i]->stdin, READ_END);
        }
        if (io_pipe(jobs[i]->stdout)) {
            process_pipe(jobs[i]->stdout, WRITE_END);
        }
    }
    check_pipes_in_out();
}

// Takes the running jobs and count as @param.
// Validating file output.
JobFileFields** validate_file_output(JobFileFields** jobs, int c, int* count) {
    int n = 0;
    JobFileFields** result = malloc(sizeof(JobFileFields *) * c);
    for (int i = 0; i < c; i++) {
        if (valid_file_out(jobs[i])) {
            result[n++] = jobs[i];
        }
    }
    free(jobs);
    *count = n;
    return result;
}

// Accepts job as @param.
// Forks the parent process that is the jobrunner.
// Child process redirects stdin, stdout, stderr and exec() a new program.
// While main method closes unused pipe end for redirection, 
// Returns newly created process's pid.
// Also redirects child process erros to /dev/null.
pid_t start_job(JobFileFields* job) {
    pid_t pid = fork();
    if (!pid) {
        int fd;
        int* pipe;
        if (io_file(job->stdin)) {
            fd = open(job->stdin, O_RDONLY);
            dup2(fd, STDIN_FILENO);
            close(fd);
        }
        if (io_file(job->stdout)) {
            fd = open(job->stdout, O_WRONLY);
            dup2(fd, STDOUT_FILENO);
            close(fd);
        }
        if (io_pipe(job->stdin)) {
            pipe = get_pipe(job->stdin);
            close(pipe[1]);
            dup2(pipe[0], STDIN_FILENO);
            close(pipe[0]);
        }
        if (io_pipe(job->stdout)) {
            pipe = get_pipe(job->stdout);
            close(pipe[0]);
            dup2(pipe[1], STDOUT_FILENO);
            close(pipe[1]);
        }
        dup2(open("/dev/null", 2, O_WRONLY), 2);
        char** argv = malloc(sizeof(char*) * (2 + job->buf));
        argv[0] = job->name;
        int i;
        for (i = 0; i < job->buf; i++) {
            argv[i + 1] = job->args[i];
        }
        argv[i+1] = NULL;
        execvp(argv[0], argv);
        exit(EXEC_FAIL);
    } else {
        int* pipe;
        if (io_pipe(job->stdin)) {
            pipe = get_pipe(job->stdin);
            close(pipe[0]);
        }
        if (io_pipe(job->stdout)) {
            pipe = get_pipe(job->stdout);
            close(pipe[1]);
        }
    }
    return pid;
}

// Takes validated jobs and count as @param.
// Starts running the validated jobs.
// Returns the current running job. 
RunningJob* run_jobs(JobFileFields** validated, int count) {
    RunningJob* running = malloc(sizeof(RunningJob) * count);
    for (int i = 0; i < count; i++) {
        running[i].job = validated[i];
        running[i].pid = start_job(validated[i]);
        running[i].completed = false;
    }
    //after_jobs_started(); // closes pipes for jobrunner
    return running;
}

// Accepts jobs and their count as @param.
// Checks job completed status and returns true if not complete.
bool check_job_status(RunningJob* jobs, int count) {
    for (int i = 0; i < count; i++) {
        if (jobs[i].completed == false) {
            return true;
        }
    }
    return false;
}

// Accepts current running jobs, count and pid as @param.
// Search jobs from pid of the current process and returns job.
RunningJob* find_from_pid(RunningJob* jobs, int count, pid_t pid) {
    for (int i = 0; i < count; i++) {
        if (jobs[i].pid == pid) {
            return &jobs[i];
        }
    }
    return NULL;
}

// Accepts current running jobs, count, pid and status as @param.
// If job is completed, then checks if exited successfully or with a signal.
// Prints the appropriate Job exited message.
void display_status(RunningJob* jobs, int count, pid_t pid, int childStatus) {
    RunningJob* job = find_from_pid(jobs, count, pid);
    if (!job) {
        return;
    }
    job->completed = true;
    if (WIFEXITED(childStatus)) {
        sprintf(exitMessages[job->job->jobNumber], 
                "Job %d exited with status %d\n", job->job->jobNumber,
                WEXITSTATUS(childStatus));

    } else if (WIFSIGNALED(childStatus)) {
        sprintf(exitMessages[job->job->jobNumber], 
                "Job %d terminated with signal %d\n", job->job->jobNumber, 
                WTERMSIG(childStatus));

    }
}

// Handles SIGHUP signal.
// Checks if the waiting job has been completed if false then kills the job.
void sighup_handler(int num) {
    for (int i = 0; i < jobCount; i++) {
        if (jobsWaiting[i].completed == false) {
            kill(jobsWaiting[i].pid, SIGKILL);
        }
    }
}

// Takes running jobs, seconds, timeout and i as @param.
// Checks if timeout occured.
// if given number of seconds >= timeout then returns true.
// If job already completed then returns false since timeout not applicable.
bool is_timeout(RunningJob* jobs, long seconds, long timeout, int i) {
    if (jobs[i].completed) {
        return false;
    }
    timeout = (long) jobs[i].job->timeout;
    if (timeout && seconds >= timeout) {
        return true;
    }
    return false;
}

// Takes running jobs, count and seconds as @param.
// If timeout reached then returns the respective job.
RunningJob* timeout_reached(RunningJob* jobs, int count, long seconds) {
    long timeout = 0;
    for (int i = 0; i < count; i++) {
        if (is_timeout(jobs, seconds, timeout, i)) {
            return &jobs[i];
        }
    }
    return NULL;
}

// Takes the running jobs and count as @param.
// Locate which process has been aborted. 
void locate_abort(long currentSeconds, RunningJob* jobs, int count) {
    RunningJob* toBeAborted = timeout_reached(jobs, count, currentSeconds);
    if (toBeAborted) {
        kill(toBeAborted->pid, SIGABRT);
        int childStatus;
        pid_t pid = waitpid(toBeAborted->pid, &childStatus, WNOHANG);
        sleep(1);
        if (pid != toBeAborted->pid) {
            kill(toBeAborted->pid, SIGKILL);
        } else {
            display_status(jobs, count, pid, childStatus);
        }
    }
}

// Takes the running jobs and count as @param.
// Waiting for jobs to be completed within set timeout.
// Aborts the job if timeout occured or job is completed. 
void waiting_for_jobs(RunningJob* jobs, int count) {
    jobsWaiting = jobs;
    jobCount = count;
    long seconds = 0;
    struct timeval startTime;
    struct timeval currentTime;
    gettimeofday(&startTime, NULL);
    do {
        int childStatus;

        pid_t pid = waitpid(-1, &childStatus, WNOHANG);
        if (pid > 0) {
            display_status(jobs, count, pid, childStatus);
        }

        gettimeofday(&currentTime, NULL);
        seconds = currentTime.tv_sec - startTime.tv_sec;
        locate_abort(seconds, jobs, count);
        
    } while (check_job_status(jobs, count));
}

int main(int argc, char** argv) {
    signal(SIGHUP, sighup_handler);
    int count = 0;
    bool vFlag;
    vFlag = check_args(argc, argv);
    int index = 1;
    if (vFlag) {
        index = 2;
    }
    if (index == argc) {
        usg_err();
    }
    char** files = argv + index;
    
    JobFileFields* jobs = process_files(files, &count);

    JobFileFields** validated;
    initialize_pipe_library(); // initialising lib before validation
    validated = validate_file_io(jobs, count, &count);
    start_pipes(validated, count);
    validated = validate_pipe_io(validated, count, &count);
    print_invalid_usages();

    if (!count) {
        invalid_jobs();
    }
    if (vFlag) {
        for (int i = 0; i < count; i++) {
            verbose_mode_display(validated[i]);
        }
    }
    // iterate and run jobs
    RunningJob* running = run_jobs(sort_jobs(validated, count), count);
    waiting_for_jobs(running, count);
    for (int i = 0; i < count; i++) {
        fprintf(stderr, "%s", exitMessages[validated[i]->jobNumber]);
    }
    return 0;
}
