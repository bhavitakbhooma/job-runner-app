#ifndef PIPE_LIB_H
#define PIPE_LIB_H

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <stdbool.h>

#define READ_END 'r'
#define WRITE_END 'w'

void initialize_pipe_library();
int* get_pipe(char* pipeName);
void process_pipe(char* pipeName, char mode);
void add_invalid_pipe(char* pipeName);
bool check_invalid_pipe(char* pipeName);
void check_pipes_in_out();
void print_invalid_usages();

#endif