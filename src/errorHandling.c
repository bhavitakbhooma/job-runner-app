// Library for handling errors.

#include "errorHandling.h"

#define USG_ERR 1
#define FILE_ERR 2
#define LINE_ERR 3
#define NO_JOB 4

// Prints Usage Error to standard error
// Terminates the program with exitstatus '1'
void usg_err() {
    fprintf(stderr, "Usage: jobrunner [-v] jobfile [jobfile ...]\n");
    exit(USG_ERR);
}

// Accepts fileName as parameter.
// Prints Invalid File Error to standard error
// Terminates the program with exitstatus '2'
void invalid_file(char* fileName) {
    fprintf(stderr, "jobrunner: file \"%s\" can not be opened\n", fileName);
    exit(FILE_ERR);
}

// Accepts linenum and fileName as parameter.
// Prints Invalid Line Error to standard error
// Terminates the program with exitstatus '3'
void invalid_line(int linenum, char* fileName) {
    fprintf(stderr, 
            "jobrunner: invalid job specification on line %d of \"%s\"\n", 
            linenum, fileName);
    exit(LINE_ERR);
}

// Prints Invalid Jobs Error to standard error
// Terminates the program with exitstatus '4'
void invalid_jobs() {
    fprintf(stderr, "jobrunner: no runnable jobs\n");
    exit(NO_JOB);
}

// Accepts fileName as parameter.
// Prints error message when jobrunner is unable 
// to open a specified stdout file for reading.
void error_stdin(char* fileName) {
    fprintf(stderr, "Unable to open \"%s\" for reading\n", fileName);
}

// Accepts fileName as parameter.
// Prints error message when jobrunner is unable 
// to open a specified stdout file for writing.
void error_stdout(char* fileName) {
    fprintf(stderr, "Unable to open \"%s\" for writing\n", fileName);
}

// Accepts pipeName as parameter.
// Prints Pipe Usage Error to standard error.
void invalid_pipe(char* pipepipeName) {
    fprintf(stderr, "Invalid pipe usage \"%s\"\n", &pipepipeName[1]);
}
