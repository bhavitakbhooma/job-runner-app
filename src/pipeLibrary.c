// Library for handling pipes.

#include "pipeLibrary.h"
#include "errorHandling.h"

#define CAPACITY 800

// Static variables are used at the scope of this c file.
// They are stored at heap memory.
static int pipes[CAPACITY][2];
// Array that stores pipeNames.
static char* pipeNames[CAPACITY]; 
// Array that stores invalidPipes.
static char* invalidPipes[CAPACITY];
// Keeps track of the read end of the pipe.
static int readers[CAPACITY];
// Keeps track of the write end of the pipe.
static int writers[CAPACITY];
// Keeps tracks of total pipes.
static int totalPipes = -1;
// Keeps track of total invalid pipes.
static int totalInvalidPipes = -1;

// Intitialising total number of pipes and invalid pipes.  
void initialize_pipe_library() {
    totalPipes = 0;
    totalInvalidPipes = 0;
}

// Prints Pipe Usage Error for all the invalid pipes.
void print_invalid_usages() {
    for (int i = 0; i < totalInvalidPipes; i++) {
        invalid_pipe(invalidPipes[i]);
    }
}

// Accepts pipeName as parameter.
// Returns the pipeNamed pipe or creates new one with that pipeName.
int* get_pipe(char* pipeName) {
    for (int i = 0; i < totalPipes; i++) {
        if (!strcmp(pipeName, pipeNames[i])) {
            return &pipes[i][0];
        }
    }
    // pipe with that pipeName not there
    pipe(pipes[totalPipes]);
    pipeNames[totalPipes] = pipeName;
    readers[totalPipes] = 0;
    writers[totalPipes] = 0;
    totalPipes++;
    return &pipes[totalPipes - 1][0];
}

// Accepts pipeName as parameter.
// Compares pipeName to see if it already exists. 
int pipe_index(char* pipeName) {
    for (int i = 0; i < totalPipes; i++) {
        if (!strcmp(pipeName, pipeNames[i])) {
            return i;
        }
    }
    return -1;
}

// Accepts pipeName as parameter and the mode (R/W) in which the pipe is.
// Increment use count for read or write mode for the pipe with name
// Also creates new pipe if that name does not exists yet.
void process_pipe(char* pipeName, char mode) {
    int index = pipe_index(pipeName);
    if (index < 0) {
        (void) get_pipe(pipeName);
        index = pipe_index(pipeName);
    }
    if (mode == READ_END) {
        readers[index]++;
    } else {
        writers[index]++;
    }
}

// Accepts pipeName as parameter.
// Adding invalid pipe to the invalidPipes array.
void add_invalid_pipe(char* pipeName) {
    invalidPipes[totalInvalidPipes++] = pipeName;
}

// Accepts pipeName as parameter.
// Checks if the pipeName is invalid.
// Returns true if it is else returns false. 
bool check_invalid_pipe(char* pipeName) {
    for (int i = 0; i < totalInvalidPipes; i++) {
        if (!strcmp(pipeName, invalidPipes[i])) {
            return true;
        }
    }
    return false;
}

// Checks if the pipe is not invalid and adds it to the list. 
void check_pipes_in_out() {
    for (int i = 0; i < totalPipes; i++) {
        if (readers[i] != 1 || writers[i] != 1) {
            if (!check_invalid_pipe(pipeNames[i])) {
                add_invalid_pipe(pipeNames[i]);
            }
        }
    }
}