#ifndef ERR_LIB_H
#define ERR_LIB_H

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

void usg_err();
void invalid_file(char* fileName);
void invalid_line(int linenum, char* fileName);
void invalid_jobs();
void error_stdin(char* fileName);
void error_stdout(char* fileName);
void invalid_pipe(char* pipepipeName);

#endif